import { Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'

import parrotHome1 from './assets/parrot-home-1.png'
import parrotHome2 from './assets/parrot-home-2.png'
import parrotHome3 from './assets/parrot-home-3.png'
import parrotHome4 from './assets/parrot-home-4.png'
import parrotHome5 from './assets/parrot-home-5.png'

import DESection from 'containers/DownloadContainers/DESection'

const useStyles = makeStyles(theme => ({
  desktopEnvironment: {
    marginTop: theme.spacing(8)
  }
}))

const OSHome = () => {
  const classes = useStyles()
  return (
    <>
      <Grid container justifyContent="center">
        <DESection
          className={classes.desktopEnvironment}
          name="Home Edition"
          description={
            <>
              Parrot Home Edition is a general purpose operating system with the typical Parrot look
              and feel. This edition is designed for daily use, privacy and software development.
              Parrot Tools can be manually installed to assemble a custom and lightweight pentesting
              environment.
            </>
          }
          version="5.1.2 Electro Ara"
          releaseDate="Nov 28, 2022"
          architecture="amd64"
          size="2.4 GB"
          download="Download"
          url="https://download.parrot.sh/parrot/iso/5.1.2/Parrot-home-5.1.2_amd64.iso"
          torrentUrl="https://download.parrot.sh/parrot/iso/5.1.2/Parrot-home-5.1.2_amd64.iso.torrent"
          torrentButton="Download Torrent"
          screenshots={[parrotHome1, parrotHome2, parrotHome3, parrotHome4, parrotHome5]}
          requirements={[
            { heading: 'Processor', description: 'Dual Core CPU' },
            { heading: 'Graphics', description: 'No Graphical Acceleration Required' },
            { heading: 'Memory', description: '1 GB RAM' },
            { heading: 'Storage', description: '16 GB available space' }
          ]}
          features={[
            {
              hero: 'Workstation',
              content: [
                {
                  heading: 'Full Office Suite',
                  description: (
                    <>
                      Pre-installed LibreOffice, and possibility to install other softwares via the
                      Synaptic package manager.
                    </>
                  )
                },
                {
                  heading: 'Multimedia Production',
                  description: (
                    <>
                      VLC, GIMP and a whole repository from which to install other software such as
                      OBS, Blender, Kdenlive, Krita and more!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Privacy',
              content: [
                {
                  heading: 'Anonymity tools',
                  description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
                },
                {
                  heading: 'Cryptography',
                  description: (
                    <>
                      Full disk encryption and all encryption tools including zulucrypt, sirikali...
                      at your fingertips!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Development',
              content: [
                {
                  heading: 'Development Tools',
                  description: <>VSCodium and Geany. You can start developing what you want.</>
                },
                {
                  heading: 'Advanced Framework Support',
                  description: (
                    <>
                      Fully support for a lot of programming languages/frameworks like Go, Rust,
                      Python and more.
                    </>
                  )
                }
              ]
            }
          ]}
          hashes={{
            md5: '5fe197d3381a4cc816eb523c32532076',
            sha1: 'b04b4ea5bcbff79d5a098cd6524296a37d2f7ee7',
            sha224: 'b907e0dc4ba156566471a8a271ddd575a7af2c5af4cc8bc18a949cc3',
            sha256: '8ed307e7a135ee3bca048abf609ed8af22ecae5fb0e1c72472d39929c7cf827a',
            sha384:
              '34e4171853fe502b3130d0c82da5af9fd15b79b910fe9a03c83aa522eea1042d40e1ca10e294259584ab5467b8be5e33',
            sha512:
              '98001b8c49f2eeb44cb354b630a43681c2c1bb6e6ad15e3c0d70fdcf8e5f85d918cd9b8afa91f546b7f93d158e98858722db17e2b7e209321dc93d2980e05cb8'
          }}
        />
      </Grid>
    </>
  )
}

export default OSHome
