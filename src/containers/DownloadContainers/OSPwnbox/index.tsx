import { Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'

import htb1 from './assets/htb-1.png'
import htb2 from './assets/htb-2.png'
import htb3 from './assets/htb-3.png'
import htb4 from './assets/htb-4.png'
import htb5 from './assets/htb-5.png'

import DESection from 'containers/DownloadContainers/DESection'

const useStyles = makeStyles(theme => ({
  desktopEnvironment: {
    marginTop: theme.spacing(8)
  },
  PwnboxTitle: {
    width: '100%',
    marginTop: theme.spacing(8),
    padding: theme.spacing(8),
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4)
    }
  },
  gridHrMarginTop: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(2)
  },
  subtitleMargined: {
    marginBottom: theme.spacing(4)
  },
  subBlockHeading: {
    fontFamily: 'museo-sans',
    fontWeight: 900
  }
}))

const OSPwnbox = () => {
  const classes = useStyles()
  return (
    <>
      <Grid container justifyContent="center">
        {/*<Paper className={classes.PwnboxTitle} elevation={0}>
          <Typography variant="h4" paragraph>
            Hack The Box Edition
          </Typography>
          <Typography variant="subtitle2Semi" paragraph>
            Unleash the full power of your Pwnbox, a customized hacking cloud box based on ParrotOS,
            on your computer or try it at{' '}
            <Link href="https://academy.hackthebox.com" underline="none">
              Hack The Box Academy
            </Link>
            .
          </Typography>
          <Typography variant="h3" paragraph align="center" sx={{ marginY: 10 }}>
            Coming Soon!
          </Typography>
          <Grid container>
            <Grid item xs={12} container spacing={4} style={{ paddingTop: 30 }}>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" paragraph>
                  Pentest-ready platform
                </Typography>
                <Typography variant="body2Semi">
                  All the hacking tools pre-installed. ParrotOS Security Edition customized with HTB
                  themes.
                </Typography>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" paragraph>
                  Beginners in Cyber Security
                </Typography>
                <Typography variant="body2Semi">
                  If you don’t know which tools you need yet or how to set up a hacking VM/OS, this
                  is the answer on how to start your hacking journey.
                </Typography>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" paragraph>
                  Slow device/laptop or with low capabilities
                </Typography>
                <Typography variant="body2Semi">
                  Your PC doesn’t have the capabilities to run a hacking VM or is broken? Pwnbox to
                  the rescue!
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Paper>*/}
        <DESection
          className={classes.desktopEnvironment}
          name="Hack The Box Edition"
          description={
            <>
              Unleash the full power of your Pwnbox, a customized hacking cloud box based on
              ParrotOS Security Edition, on your computer or try it online at Hack The Box Academy.
            </>
          }
          version="5.1.2 Electro Ara"
          releaseDate="Nov 28, 2022"
          architecture="amd64"
          size="4.3 GB"
          download="Download"
          url="https://download.parrot.sh/parrot/iso/5.1.2/Parrot-htb-5.1.2_amd64.iso"
          torrentUrl="https://download.parrot.sh/parrot/iso/5.1.2/Parrot-htb-5.1.2_amd64.iso.torrent"
          torrentButton="Download Torrent"
          screenshots={[htb1, htb2, htb3, htb4, htb5]}
          requirements={[
            { heading: 'Processor', description: 'Dual Core CPU' },
            { heading: 'Graphics', description: 'No Graphical Acceleration Required' },
            { heading: 'Memory', description: '1 GB RAM' },
            { heading: 'Storage', description: '20 GB available space' }
          ]}
          features={[
            {
              hero: 'Use cases',
              content: [
                {
                  heading: 'Beginners in Cyber Security',
                  description: (
                    <>
                      If you don’t know which tools you need yet or how to set up a hacking VM/OS,
                      this is the answer on how to start your hacking journey.
                    </>
                  )
                },
                {
                  heading: 'Slow device/laptop or with low capabilities',
                  description: (
                    <>
                      Your PC doesn’t have the capabilities to run a hacking VM or is broken? Hack
                      The Box Edition to the rescue!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Development',
              content: [
                {
                  heading: 'Development Tools',
                  description: <>VSCodium and Geany. You can start developing what you want.</>
                },
                {
                  heading: 'Advanced Framework Support',
                  description: (
                    <>
                      Fully support for a lot of programming languages/frameworks like Go, Rust,
                      Python and more.
                    </>
                  )
                }
              ]
            }
          ]}
          hashes={{
            md5: '0c2242b33a25c056fc678de7d9cd1e11',
            sha1: '677e2126cae16b2be1b4e6510c9213335cf4adf9',
            sha224: '2bef87c772ec4deed2977f9dc422fad25b14edb0c13aadda3be41412',
            sha256: 'dc6c12c203fd2525f118b310772c62819f729a3be8ba4146cd3cf801d21bf72d',
            sha384:
              '2e2cc7236296290045e1610d148fcaf8ae26f5500bd6396c79788d33b5119ea0410f7bf92e12ad2c321dc8683e2f8f11',
            sha512:
              '42e9ad85895dff1fc0ca50e9e08b91dc151722df0d91327822effaf683f7d02861a1f413e31460d0e29bbf44d214c7cc83324b39c60f8e41f357ada6f4e4abe4'
          }}
        ></DESection>
      </Grid>
    </>
  )
}

export default OSPwnbox
