import { Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'

import parrotSecurity1 from './assets/parrot-security-1.png'
import parrotSecurity2 from './assets/parrot-security-2.png'
import parrotSecurity3 from './assets/parrot-security-3.png'
import parrotSecurity4 from './assets/parrot-security-4.png'
import parrotSecurity5 from './assets/parrot-security-5.png'

import DESection from 'containers/DownloadContainers/DESection'

const useStyles = makeStyles(theme => ({
  desktopEnvironment: {
    marginTop: theme.spacing(8)
  }
}))

const OSSecurity = () => {
  const classes = useStyles()
  return (
    <>
      <Grid container justifyContent="center">
        <DESection
          className={classes.desktopEnvironment}
          name="Security Edition"
          description={
            <>
              Parrot Security Edition is a special purpose operating system designed for Penetration
              Test and Red Team operations. It contains a full arsenal of ready to use pentesting
              tools.
            </>
          }
          version="5.1.2 Electro Ara"
          releaseDate="Nov 28, 2022"
          architecture="amd64"
          size="4.8 GB"
          download="Download"
          url="https://download.parrot.sh/parrot/iso/5.1.2/Parrot-security-5.1.2_amd64.iso"
          torrentUrl="https://download.parrot.sh/parrot/iso/5.1.2/Parrot-security-5.1.2_amd64.iso.torrent"
          torrentButton="Download Torrent"
          screenshots={[
            parrotSecurity1,
            parrotSecurity2,
            parrotSecurity3,
            parrotSecurity4,
            parrotSecurity5
          ]}
          requirements={[
            { heading: 'Processor', description: 'Dual Core CPU' },
            { heading: 'Graphics', description: 'No Graphical Acceleration Required' },
            { heading: 'Memory', description: '1 GB RAM' },
            { heading: 'Storage', description: '20 GB available space' }
          ]}
          features={[
            {
              hero: 'Workstation',
              content: [
                {
                  heading: 'Full Office Suite',
                  description: (
                    <>
                      Pre-installed LibreOffice, and possibility to install other softwares via the
                      Synaptic package manager.
                    </>
                  )
                },
                {
                  heading: 'Multimedia Production',
                  description: (
                    <>
                      Pre-installed VLC, GIMP and a whole repository from which to install other
                      software such as OBS, Blender, Kdenlive, Krita and more!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Privacy',
              content: [
                {
                  heading: 'Anonymity tools',
                  description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
                },
                {
                  heading: 'Cryptography',
                  description: (
                    <>
                      Full disk encryption and all encryption tools including zulucrypt, sirikali...
                      at your fingertips!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Development',
              content: [
                {
                  heading: 'Pentest-ready',
                  description: (
                    <>
                      Lots of penetration testing tools, all already installed, including
                      Powersploit, Scapy, Rizin and more!
                    </>
                  )
                },
                {
                  heading: 'Development Tools',
                  description: <>VSCodium and Geany. You can start developing what you want.</>
                }
              ]
            }
          ]}
          hashes={{
            md5: '240ca91a0b340a452a6dd9f4b17b9f92',
            sha1: 'd6c74e93a7da9cb14a2239b75ec07045df42f097',
            sha224: '204a1a34fe28eb54c8e94a5983b4afb4bb49ab3190f6dac975ea8e47',
            sha256: '54e48377ed9e44fe3aef0b2bdd2b12b1cce596b29c27ddeca693d70c571d990c',
            sha384:
              '42e6ca48a340175e4de187daca8bc31b9a1bdd6f693894544ba1d203d91458948121384f42e37662053743e134d75345',
            sha512:
              '4dca38e248209102ba5847ff70671b0eb4204ac638f576f8071e7c189f7dc10cf6c400918eaf1d8e7b95a8fbce2d6e0429e9f78bc4403ff081f6dda1d969dd81'
          }}
        />
      </Grid>
    </>
  )
}

export default OSSecurity
