# ParrotOS Website

Written in React and Typescript. It has a Makefile where you can test it locally. 

You can try it by using the `npm run dev` command.
